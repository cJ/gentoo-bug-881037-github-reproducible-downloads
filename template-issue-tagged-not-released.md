
v2.4.2; it is packaged in Gentoo Linux Science overlay (the automatic tag tarball is used as a source when users install the package).
A problem (see https://bugs.gentoo.org/881037) is that github provides no guarantee that this tarball will be identical when downloaded many times; this creates a problem when a distributor has software supply chain quality considerations and adds integrity checking to the download.
If you could please release this version, the potential problem would go away.
