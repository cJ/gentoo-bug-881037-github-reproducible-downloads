#!/usr/bin/env python
# -*- coding: utf-8 vi:noet
# SPDX-FileCopyrightText: 2022 Jérôme Carretero <cJ-gentoo@zougloub.eu> & contributors
# SPDX-License-Identifier: MIT

if 0:
	# In case we want to use a proxy which can help with our mass github requests...

	import socks
	import socket
	import http.client

	def create_connection(*args, **kw):
		conn = socks.create_connection(*args,
		 proxy_type=socks.SOCKS5,
		 proxy_addr="127.0.0.1",
		 proxy_port=5000,
		 **kw)
		return conn

	socket.create_connection = create_connection

import sys, io, os, logging
import collections
import argparse
import json
import re
import subprocess
import urllib.request, urllib.error
import xml.etree.ElementTree as ET
import hashlib
import multiprocessing

import yaml


logger = logging.getLogger(__name__)


def get_parser():
	parser = argparse.ArgumentParser(
	 description="Check github stuff",
	)

	parser.add_argument("--log-level",
	 default="INFO",
	 help="logger level (eg. INFO, see Python logger docs)",
	)

	subparsers = parser.add_subparsers(
	 help='the command; type "%s COMMAND -h" for command-specific help' % sys.argv[0],
	 dest='command',
	)

	return parser


def ebuild_uris(path):
	path = path[2:] if path.startswith("./") else path

	with io.open(path, "rb") as fi:
		h = hashlib.md5()
		h.update(path.encode("utf-8"))
		h.update(fi.read())

	cache_fn = f".cache/{h.hexdigest()}"
	if "cadquery" in path and 0:
		pass
	elif os.path.exists(cache_fn):
		with io.open(cache_fn, "r", encoding="utf-8") as fi:
			return json.load(fi)

	cmd = [
	 "pquery",
	 path,
	 "--attr", "uris"
	]
	ret = subprocess.run(cmd,
	 stdout=subprocess.PIPE,
	 stderr=subprocess.PIPE,
	)

	logger.debug("Ret: %s", ret)

	s = ret.stdout.rstrip().decode("utf-8")

	m = re.match(r'\S+ uris="(?P<uris>.*)"', s)
	if m is None:
		ret = []
	else:
		ret = m.group("uris").split()

	with io.open(cache_fn, "w", encoding="utf-8") as fo:
		json.dump(ret, fo)

	return ret


def github_releases(github_repo):

	h = hashlib.md5()
	h.update(github_repo.encode("utf-8"))

	cache_fn = f".cache/{h.hexdigest()}"
	if os.path.exists(cache_fn):
		with io.open(cache_fn, "r", encoding="utf-8") as fi:
			return json.load(fi)

	url = f"https://api.github.com/repos/{github_repo}/releases"
	#url = f"https://api.github.com/repos/{github_repo}/releases	/{release_id}"

	gh_token = os.environ["GITHUB_TOKEN"]

	headers = {
	 "Authorization": f"token {gh_token}",
	 #"Content-Type": "application/octet-stream",
	}

	req = urllib.request.Request(url, headers=headers, method="GET")
	try:
		with urllib.request.urlopen(req) as resp:
			if resp.getcode() != 200:
				logger.warning("%s: bad response %s %s", github_repo, resp.getcode(), resp.read())
				ret = []
			else:
				ret = json.loads(resp.read().decode("utf-8"))
	except Exception as e:
		ret = [] # TODO don't except

	with io.open(cache_fn, "w", encoding="utf-8") as fo:
		json.dump(ret, fo)

	return ret


def lookup_package_urls(f):
	"""
	"""

	github_uris = set()

	with io.open(f, "r", encoding="utf-8") as fi:
		for line in fi:
			line = line.rstrip()
			m = re.search(r"(?P<url>https://github.com.*archive/v?\$\{PV\}(.tar.gz|.zip))", line)
			m = re.search(r"(?P<url>https://github.com.*archive/\S+)", line)
			if m is None:
				continue

			logger.debug("Got github: %s", m.group("url"))

			pn = os.path.basename(os.path.dirname(f))
			category = os.path.basename(os.path.dirname(os.path.dirname(f)))

			github_uris = set()
			uris = ebuild_uris(f)
			for uri in uris:
				m = re.search(r"(?P<url>https://github.com.*archive/\S+)", uri)
				if m is None:
					continue
				github_uris.add(uri)

	if github_uris:
		p = f"{category}/{pn}"
		return p, github_uris


def lookup_packages_and_urls():
	"""
	"""

	out = collections.defaultdict(lambda: set())
	ebuilds = collections.defaultdict(lambda: set())

	with multiprocessing.Pool() as pool:

		res = dict()
		for cwd, ds, fs in os.walk("."):
			ds.sort()
			fs.sort()
			for fn in fs:
				if not fn.endswith(".ebuild"):
					continue

				f = os.path.join(cwd, fn)
				res[f] = fn, pool.apply_async(lookup_package_urls, (f,))

		for f, (fn, x) in res.items():
			res = x.get()
			if res is None:
				continue
			p, uris = res

			for uri in uris:
				out[p].add(uri)

			ebuilds[p].add(f"{p}/{fn}")

	return ebuilds, out


def lookup_packages_github_repo():
	"""
	Find github repo for packages
	"""

	out = dict()

	ebuilds, d = lookup_packages_and_urls()

	#class CommentedTreeBuilder(ET.TreeBuilder):
	#	def comment(self, data):
	#		self.start(ET.Comment, {})
	#		self.data(data)
	#		self.end(ET.Comment)


	for p, urls in d.items():

		repos = set()
		for url in urls:
			m = re.search(r"https://github.com/(?P<repo>[^/]+/[^/]+)", url)
			assert m is not None, url
			repo = m.group("repo")
			repos.add(repo)

		if len(repos) > 1:
			logger.info("%s: Skipping as many repos: %s", p, repos)
			continue
		elif len(repos) == 0:
			logger.info("%s: couldn't infer github repo", p)
			continue

		repo_ = tuple(repos)[0]

		out[p] = repo_

		mf = os.path.join(p, "metadata.xml")

		parser = ET.XMLParser(target=ET.TreeBuilder(insert_comments=True, insert_pis=True))
		tree = ET.parse(mf, parser=parser)
		root = tree.getroot()

		try:
			repo = root.find("./upstream/remote-id[@type=\"github\"]").text
			if repo != repo_:
				logger.warning("%s repo differs: SRC_URI said %s but metadata.xml said %s", p, repo_, repo)

		except AttributeError as e:
			upstream = root.find("./upstream")
			if upstream is None:
				upstream = ET.SubElement(root, "upstream")

			remote = ET.SubElement(upstream, "remote-id")
			remote.attrib["type"] = "github"

			remote.text = repo_

			ET.indent(tree, "\t")

			with io.open(mf, "w", encoding="UTF-8") as fo:
				fo.write("""<?xml version="1.0" encoding="UTF-8"?>\n<!DOCTYPE pkgmetadata SYSTEM "https://www.gentoo.org/dtd/metadata.dtd">\n""")
				fo.write(ET.tostring(root,
				 encoding="unicode",
				) + "\n")

			logger.warning("Corrected %s", p)

	return out


def lookup_proper_release_url(p, ebuilds, github_repo):
	#if not "meshpy" in p:
	#	return
	logger.info("Package %s", p)
	ebuild2auto = dict()
	for ebuild in ebuilds:
		uris = ebuild_uris(ebuild)
		logger.info("ebuild %s uris %s", ebuild, uris)
		for uri in uris:
			if "github" in uri:
				ebuild2auto[ebuild] = os.path.basename(uri).replace(".zip", "").replace(".tar.gz", "")
			else:
				logger.debug("Ignoring %s", uri)

	logger.info("ebuild and uris: %s", ebuild2auto)

	releases = set()
	found_ebuilds = dict()
	found_releases = dict()

	ret = github_releases(github_repo)
	#logger.info("ret: %s", ret)
	for release in ret:
		#logger.info("ret: %s", release)
		url = release["url"]
		release_name = release["name"]
		if release_name is None:
			release_name = "???"
		tagname = release["tag_name"]
		assets = release["assets"]
		releases.add(release_name)
		if not assets:
			logger.info("%s release %s has no assets", p, release_name)

		found = False
		for asset in assets:
			logger.info("%s %s %s", p, tagname, asset["name"])

			for ebuild, auto in ebuild2auto.items():
				if asset["name"] == auto:
					found = ebuild
					found_ebuilds[ebuild] = release_name

		if found:
			found_releases[release_name] = True

	for ebuild in ebuilds:
		if not ebuild in found_ebuilds:
			logger.warning("Couldn't resolve URI for %s : %s (releases: %s)",
			 ebuild, ebuild2auto[ebuild], sorted(tuple(releases)))

def update_manifest(path):
	logger.info("Updating manifest for %s", path)

	try:
		if 0:
			mf = os.path.join(path, "Manifest")
			mf2 = os.path.join(path, "Manifest-mirrors")
			if os.path.exists(mf):
				os.unlink(mf)
			if os.path.exists(mf2):
				os.unlink(mf2)
			cmd = [
			 "pkgdev",
			  "manifest",
			  path,
			  "--verbose",
			  "--distdir",
			  os.path.expanduser("~/distdir-mirrors"),
			  "--mirrors",
			  "--ignore-fetch-restricted",
			]
			ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			if ret.returncode == 0:
				logger.warning("%s: manifested", path)
				os.rename(mf, mf2)
			else:
				logger.warning("%s: Failed to manifest: %s", path, ret)

		if 1:
			mf = os.path.join(path, "Manifest")
			mf2 = os.path.join(path, "Manifest-upstream")
			if os.path.exists(mf):
				os.unlink(mf)
			if os.path.exists(mf2):
				os.unlink(mf2)

			cmd = [
			 "pkgdev",
			  "manifest",
			  path,
			  "--verbose",
			  "--distdir",
			  os.path.expanduser("~/distdir"),
			  "--ignore-fetch-restricted",
			]
			ret = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
			if ret.returncode == 0:
				logger.warning("%s: manifested", path)
				os.rename(mf, mf2)
			else:
				logger.warning("%s: Failed to manifest: %s", path, ret)
	except Exception as e:
		logger.exception("%s: failed to manifest: %s", path, e)


def main(args_in=None):

	if 0:
		proxy_support = urllib.request.ProxyHandler()
		opener = urllib.request.build_opener(proxy_support)
		urllib.request.install_opener(opener)

	parser = get_parser()

	try:
		import argcomplete
		argcomplete.autocomplete(parser)
	except:
		pass

	args = parser.parse_args(args=args_in)

	logging.basicConfig(
	 level=getattr(logging, args.log_level),
	)

	try:
		import coloredlogs
		coloredlogs.install(
		 level=getattr(logging, args.log_level),
		)
	except ImportError:
		pass

	p2ebuilds, d = lookup_packages_and_urls()
	d = lookup_packages_github_repo()

	for p, ebuilds in p2ebuilds.items():
		try:
			repo = d[p]
			print(f"{p} at https://github.com/{repo}")
		except:
			logger.info("Couldn't get repo for %s / %s", p, ebuilds)
			continue

		lookup_proper_release_url(p, ebuilds, repo)

	if 1:
		with multiprocessing.Pool() as pool:

			res = []
			for p, ebuilds in p2ebuilds.items():
				try:
					repo = d[p]
					print(f"{p} at https://github.com/{repo}")
				except:
					logger.info("Couldn't get repo for %s / %s", p, ebuilds)
					continue
				x = pool.apply_async(update_manifest, (p,))
				res.append(x)

				# TODO complain about archive/v URLs which can be ambiguous (eg. easy-rsa 3.0.5)
				# TODO match URLs with githubusercontent.com.*master, github.com.*master

			for x in res:
				x.get()

	if 1:
		# Check size variation
		pass

if __name__ == "__main__":
	ret = main()
	raise SystemExit(ret)
